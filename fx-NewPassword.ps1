﻿function New-Password {
	[CmdletBinding()]
	Param
	(
		[Parameter(Mandatory=$false,
		HelpMessage="Specify the full file path to the secure string file.")]
		[Int32] $Length = 8,
		
		[Parameter(Mandatory=$false,
		HelpMessage="Specify the new password strength.")]
		[ValidateSet('Weak', 'Moderate', 'Strong')]
		[String] $Strength = "Moderate"
	)
	Begin
	{

	}
	Process
	{
		# Declare and initialize variables
		[Object] $characterBytes = $null;
		[Object] $output = New-Object Management.Automation.PSObject;
		[String] $passwordCharacters = $null;
		[Object] $randomizer = $null;
		[String] $result = $null;
		
		# Determine the characters based on strength
		switch ($Strength)
		{
			"Weak"
			{
				$passwordCharacters = "abcdefghijkmnopqrstuvwxyz23456789";
			}
			"Moderate"
			{
				$passwordCharacters = "ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz23456789";
			}
			"Strong"
			{
				$passwordCharacters = "ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz23456789_!@#$%^&*()_";
			}
		}
		
		# Create a byte array long enough to contain the new string
		$characterBytes = New-Object "Byte[]" $Length;
		
		# Create a randomizer
		$randomizer = New-Object Security.Cryptography.RNGCryptoServiceProvider;
		$randomizer.GetBytes($characterBytes);
		
		# Select a character at random until specified length is reached
		for ( $i=0; $i -lt $Length; $i++ )
		{
			# Append the newly selected character to the new value
			$result += $passwordCharacters[ $characterBytes[$i] % $passwordCharacters.Length ];	
		}
		
		# Add result to the output object
		$output | Add-Member -MemberType NoteProperty -Name Length -Value $Length;
		$output | Add-Member -MemberType NoteProperty -Name Strength -Value $Strength;
		$output | Add-Member -MemberType NoteProperty -Name Result -Value $result;
		
		# Add a method to return the value 'ToString()'	
		$output | Add-Member -MemberType ScriptMethod -Name ToString -Value { $this.Result; } -Force;
		
		# Write the output object
		Write-Output -InputObject $output;
	} 
} 